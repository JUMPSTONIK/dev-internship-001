// import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import HeroesPage from "./components/HeroesPage";

const divRoot = document.querySelector("#root");
// insertar aqui el componente principal
ReactDOM.render(<HeroesPage />, divRoot);
